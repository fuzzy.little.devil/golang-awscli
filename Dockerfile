ARG GO_TAG
FROM golang:$GO_TAG
LABEL description="Golang + AWS CLI"
MAINTAINER fuzzy.little.devil@gmail.com
RUN apt-get update && \
    apt-get install -y zip openssl && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm awscliv2.zip && \
    rm -rf /var/lib/apt/lists/*

